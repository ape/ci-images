[![pipeline status](https://gitlab.com/ape/ci-images/badges/main/pipeline.svg)](https://gitlab.com/ape/ci-images/-/commits/main) 
[![Latest Release](https://gitlab.com/ape/ci-images/-/badges/release.svg)](https://gitlab.com/ape/ci-images/-/releases)

# APE - CI Docker Images

Collection of Docker images used by the APE CI.
